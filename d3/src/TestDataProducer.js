var testData = require('./data.json');

var TestDataProducer = {};

TestDataProducer.retrieveData = function() {

  var dataSet = {label: 'Findings', values: []};
  for (var i = 0; i < testData.findings.length; i++) {
    var finding = testData.findings[i];
    dataSet.values.push({x: finding.label, y: finding.all.absolute});
  }

  return dataSet;
};

TestDataProducer.retrieveSexData = function() {
  var dataSet = {label: 'Sex', values: []};
  for (var i = 0; i < testData.sex.length; i++) {
    var sex = testData.sex[i];
    dataSet.values.push({x: sex.label, y: sex.all.absolute});
  }

  return dataSet;
};

module.exports = TestDataProducer;
