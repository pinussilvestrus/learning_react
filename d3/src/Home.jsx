var React = require("react");
var d3 = require("d3");

var BarChart = require('./rd3lib/BarChart');
var PieChart = require('./rd3lib/PieChart');

var TestDataProducer = require('./TestDataProducer');

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    var data = [];
    data.push(TestDataProducer.retrieveData());

    var pieData = TestDataProducer.retrieveData();

    var sexData = [];
    sexData.push(TestDataProducer.retrieveSexData());

    var tooltip = function(x, y) {
      return y;
    };

    var medxBlue = "#57c1c4";

    var sort = null;

    return (

      <div>
        <p>
          Findings BarChart
        </p>
        <BarChart
          data={data}
          width={400}
          height={400}
          fill={medxBlue}
          margin={{top: 10, bottom: 50, left: 50, right: 10}}/>
        <p>
          Findings PieChart
        </p>
        <PieChart
          data={pieData}
          width={600}
          height={400}
          tooltipHtml={tooltip}
          margin={{top: 10, bottom: 10, left: 100, right: 100}}
          sort={sort}/>
          <p>
            Sex BarChart
          </p>
          <BarChart
            groupedBars
            data={sexData}
            width={400}
            height={400}
            fill={medxBlue}
            margin={{top: 10, bottom: 50, left: 50, right: 10}}/>
      </div>
    );
  }
}

export default Home;
